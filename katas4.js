const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";


function kata1() {
    
    let resultado = gotCitiesCSV.split(',');
   
    let header = document.createElement("div");
    header.textContent = "Kata 1";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return gotCitiesCSV;
    return resultado;
}
kata1();


function kata2() {
    
    let resultado = bestThing.split(' ');
   
    let header = document.createElement("div");
    header.textContent = "Kata 2";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata2();


function kata3() {
    
    let resultado = gotCitiesCSV.split(',').join(';');
   
    let header = document.createElement("div");
    header.textContent = "Kata 3";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return gotCitiesCSV;
    return resultado;

}
kata3();


function kata4() {
    
    let resultado = lotrCitiesArray.join();
   
    let header = document.createElement("div");
    header.textContent = "Kata 4";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata4();

function kata5() {
    
    let resultado = lotrCitiesArray.slice(0,5);
    
    let header = document.createElement("div");
    header.textContent = "Kata 5";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;
    
}
kata5();


function kata6() {
    
    let resultado = lotrCitiesArray.slice(-5);
     
    let header = document.createElement("div");
    header.textContent = "Kata 6";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata6();

function kata7() {
    
    let resultado = lotrCitiesArray.slice(2,5);
     
    let header = document.createElement("div");
    header.textContent = "Kata 7";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata7();

function kata8() {
    
    let resultado = lotrCitiesArray.splice(2,1);
    resultado = lotrCitiesArray;
     
    let header = document.createElement("div");
    header.textContent = "Kata 8";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata8();

function kata9() {
    
    let resultado = lotrCitiesArray.splice(-2,Number.MAX_VALUE);
    resultado = lotrCitiesArray;
     
    let header = document.createElement("div");
    header.textContent = "Kata 9";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata9();

function kata10() {
    
    let resultado = lotrCitiesArray.splice(2,0,"Rohan");
    resultado = lotrCitiesArray;
     
    let header = document.createElement("div");
    header.textContent = "Kata 10";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata10();

function kata11() {
    
    let resultado = lotrCitiesArray.splice(5,1,"Deadest Marshes");
    resultado = lotrCitiesArray;
     
    let header = document.createElement("div");
    header.textContent = "Kata 11";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata11();

function kata12() {
    
    let resultado = bestThing.slice(0,14);
         
    let header = document.createElement("div");
    header.textContent = "Kata 12";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata12();

function kata13() {
    
    let resultado = bestThing.slice(-12);
         
    let header = document.createElement("div");
    header.textContent = "Kata 13";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata13();

function kata14() {
    
    let resultado = bestThing.slice(23,38);
         
    let header = document.createElement("div");
    header.textContent = "Kata 14";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata14();

function kata15() {
    
    let resultado = bestThing.substring(69,81);
         
    let header = document.createElement("div");
    header.textContent = "Kata 15";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata15();

function kata16() {
    
    let resultado = bestThing.substring(23,38);
         
    let header = document.createElement("div");
    header.textContent = "Kata 16";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata16();

function kata17() {
    
    let resultado = lotrCitiesArray.pop();
    resultado = lotrCitiesArray;
         
    let header = document.createElement("div");
    header.textContent = "Kata 17";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata17();

function kata18() {
    
    let resultado = lotrCitiesArray.push("Deadest Marshes");
    resultado = lotrCitiesArray;
         
    let header = document.createElement("div");
    header.textContent = "Kata 18";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata18();

function kata19() {
    
    let resultado = lotrCitiesArray.shift();
    resultado = lotrCitiesArray;
         
    let header = document.createElement("div");
    header.textContent = "Kata 19";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata19();

function kata20() {
    
    let resultado = lotrCitiesArray.unshift("Mordor");
    resultado = lotrCitiesArray;
         
    let header = document.createElement("div");
    header.textContent = "Kata 20";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return lotrCitiesArray;
    return resultado;

}
kata20();

function kata21() {
    
    let resultado = bestThing.indexOf("only");
             
    let header = document.createElement("div");
    header.textContent = "Kata 21";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata21();

function kata22() {
    
    let resultado = bestThing.indexOf("bit");
             
    let header = document.createElement("div");
    header.textContent = "Kata 22";
    document.body.appendChild(header);    

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(resultado);
    document.body.appendChild(newElement)

    // return bestThing;
    return resultado;

}
kata22();









